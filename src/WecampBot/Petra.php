<?php

declare(strict_types=1);

namespace W3C;

use PhpSlackBot\Command\BaseCommand;

final class Petra extends BaseCommand
{
    protected function configure()
    {
        $this->setName('!petra');
    }

    protected function execute($message, $context)
    {
        $this->send($this->getCurrentChannel(), null, 'https://www.youtube.com/watch?v=wmin5WkOuPw');
    }
}
