<?php

declare(strict_types=1);

namespace W3C;

use PhpSlackBot\Command\BaseCommand;

final class Erik extends BaseCommand
{
    protected function configure()
    {
        $this->setName('!erik');
    }

    protected function execute($message, $context)
    {
        $this->send($this->getCurrentChannel(), null, 'https://www.youtube.com/watch?v=Bn7lDGCO32s');
    }
}
