<?php

declare(strict_types=1);

namespace W3C;

use PhpSlackBot\Command\BaseCommand;

final class Gitlab extends BaseCommand
{
    protected function configure()
    {
        $this->setName('!gitlab');
    }

    protected function execute($message, $context)
    {
        $this->send($this->getCurrentChannel(), null, 'You can access our gitlab at https://gitlab.weca.mp:6443. You can log in with your Github authentication.');
    }
}
