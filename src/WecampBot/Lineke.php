<?php

declare(strict_types=1);

namespace W3C;

use PhpSlackBot\Command\BaseCommand;

final class Lineke extends BaseCommand
{
    protected function configure()
    {
        $this->setName('!lineke');
    }

    protected function execute($message, $context)
    {
        $this->send($this->getCurrentChannel(), null, 'https://www.youtube.com/watch?v=lcOxhH8N3Bo');
    }
}
