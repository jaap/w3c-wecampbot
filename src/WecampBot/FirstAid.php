<?php

declare(strict_types=1);

namespace W3C;

use PhpSlackBot\Command\BaseCommand;

final class FirstAid extends BaseCommand
{
    protected function configure()
    {
        $this->setName('!firstaid');
    }

    protected function execute($message, $context)
    {
        $this->send($this->getCurrentChannel(), null, 'Marjolein: +31643175210, Monique: +31620820810, backup: Erik (coach): +31618365573 ');
    }
}
